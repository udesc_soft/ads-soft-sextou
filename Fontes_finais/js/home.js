(function ($) {
    $(document).ready(function(){
        
        $(".navbar").hide();
        
        $(function () {
            $('body').scroll(function () {
                if ($(this).scrollTop() > 100) {
                    $('.navbar').fadeIn();
                } else {
                    $('.navbar').fadeOut();
                }
            });
        });

        $('.scroll').click(function(e) {
            var sectionTo = $(this).attr('href');
            $('html, body').animate({
                scrollTop: $(sectionTo).offset().top
            }, 1300);            
	e.preventDefault();
        });  
	       
    });
}(jQuery));
